﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FIrstCapacity : MonoBehaviour
{
    public GameObject tentacle;
    public GameObject particuleHeal , particuleBoost , circle , caps;
    public Animator _animator;
    void Start()
    {
        tentacle.GetComponent<SkinnedMeshRenderer>().material.DOFade(0f, 1f);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            fade();
            _animator.SetTrigger("StartBool");
            StartCoroutine(Cpieuvre());
        }
    }
    IEnumerator Cpieuvre()
    {
        StartCoroutine(shake());
        tentacle.SetActive(true);
        yield return new WaitForSeconds(1f);
        tentacle.SetActive(false);
    }

    IEnumerator shake()
    {
        yield return new WaitForSeconds(0.6f);
        caps.transform.DOShakePosition(1f, 1f, 5, 1f);
    }

    public void fade()
    {
        tentacle.GetComponent<SkinnedMeshRenderer>().material.DOFade(0.0f, 5.0f);
    }
}

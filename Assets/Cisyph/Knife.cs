﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Knife : MonoBehaviour
{
    public Rigidbody rb;
    
    public bool collide;
    public bool touched;
    public GameObject particule;

    public GameObject player;

    public bool goBack;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Skill2") && touched)
        {
            rb.velocity = Vector3.zero;
            goBack = true;
            Instantiate(particule, transform.position, Quaternion.identity);
            particule.GetComponent<ParticleSystem>().Play();
            collide = false;
        }

        if (goBack)
        {
             transform.position = Vector3.MoveTowards(transform.position, player.transform.position, 1) ;
        }
        
        if (collide)
        {
            rb.velocity = transform.forward * 750 * Time.deltaTime;
        }

        if (touched)
        {
            var  trail = GetComponent<TrailRenderer>() ;
            var timer = GetComponent<Timer>();
            timer.enabled = true;
          trail.enabled = false;
        
        }

    
        
    }

    private void OnCollisionEnter(Collision other)
    {
        Debug.Log("eld");
        collide = false;
        if (other.gameObject.CompareTag("player"))
        {
            transform.SetParent(other.transform);
            touched = true;
            Instantiate(particule, gameObject.transform.position, Quaternion.identity);
            particule.GetComponent<ParticleSystem>().Play();
        }
        else if (other.gameObject.CompareTag("player") == false)
        {
            Destroy(gameObject);
        }
    }
}
